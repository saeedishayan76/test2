package ir.shayan

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class UserViewModel : ViewModel() {
    val isFilterLiveData = MutableLiveData<String>()


    fun sample() = viewModelScope.launch {
        isFilterLiveData.value = "Test"
    }

    fun setFilter(filter: Filter) {
        isFilterLiveData.value = filter.name
    }


    enum class Filter {
        ASC, DESC
    }
}