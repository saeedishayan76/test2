package ir.shayan.okunittest.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.shayan.okunittest.data.local.ShopItem
import ir.shayan.okunittest.data.remote.ApiService
import ir.shayan.okunittest.data.remote.ImageResultDto
import ir.shayan.okunittest.respository.ShopRepository
import ir.shayan.okunittest.vo.Event
import ir.shayan.okunittest.vo.Resource
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ShoppingViewModel @Inject constructor(
    private val shopRepository: ShopRepository
) : ViewModel() {

    private val shopItems = shopRepository.getAllShoppingItem()
    val insertShoppingItemStatus = MutableLiveData<Event<Resource<ShopItem>>>()
    private val images = MutableLiveData<Resource<ImageResultDto>>()

    private val currentImage = MutableLiveData<String>()

    fun setCurrentImage(url: String) {
        currentImage.value = url
    }

    fun deleteShopItem(shopItem: ShopItem) = viewModelScope.launch {
        shopRepository.delete(shopItem)
    }

    fun insertIntoDb(shopItem: ShopItem) = viewModelScope.launch {
        shopRepository.insert(shopItem)
    }

    fun insertShoppingItem(name: String, amount: String, price: String) {
        if (name.isEmpty() || amount.isNullOrEmpty()) {
            insertShoppingItemStatus.postValue(
                Event(
                    Resource.error(
                        msg = "field is empty",
                        data = null
                    )
                )
            )
            return
        }
        if (amount.toInt() > 30) {
            insertShoppingItemStatus.postValue(
                Event(
                    Resource.error(
                        msg = "big amount",
                        data = null
                    )
                )
            )
            return
        }


    }


}