package ir.shayan.okunittest.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ir.shayan.okunittest.ShopRepositoryImpl
import ir.shayan.okunittest.data.local.AppDataBase
import ir.shayan.okunittest.data.local.ShopItemDao
import ir.shayan.okunittest.data.remote.ApiService
import ir.shayan.okunittest.respository.ShopRepository
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideApiService(): ApiService = Retrofit.Builder()
        .baseUrl("https://pixabay.com")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiService::class.java)


    @Provides
    @Singleton
    fun provideAppDatabase(
        @ApplicationContext context: Context
    ): AppDataBase {
        return Room.databaseBuilder(context, AppDataBase::class.java, "db_unit").build()
    }

    @Provides
    @Singleton
    fun provideShopItemDao(appDataBase: AppDataBase): ShopItemDao = appDataBase.shopItemDao()


    @Provides
    @Singleton
    fun provideShopRepo(
        apiService: ApiService,
        shopItemDao: ShopItemDao
    ): ShopRepository {
        return ShopRepositoryImpl(apiService, shopItemDao)
    }
}