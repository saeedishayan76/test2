package ir.shayan.okunittest

object ValidateUtil {

    fun validateForSignup(username: String, pass: String, confirmPass: String): Boolean {
        if (username.isNullOrEmpty() || pass.isNullOrEmpty() || confirmPass.isNullOrEmpty())
            return false
        if (pass != confirmPass)
            return false
        return true
    }
}