package ir.shayan.okunittest

import androidx.lifecycle.LiveData
import ir.shayan.okunittest.data.local.ShopItem
import ir.shayan.okunittest.data.local.ShopItemDao
import ir.shayan.okunittest.data.remote.ApiService
import ir.shayan.okunittest.data.remote.ImageResultDto
import ir.shayan.okunittest.respository.ShopRepository
import ir.shayan.okunittest.vo.Resource
import javax.inject.Inject

class ShopRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    private val shopItemDao: ShopItemDao
) : ShopRepository {
    override suspend fun insert(shopItem: ShopItem) {
        shopItemDao.insertShopItem(shopItem)
    }

    override suspend fun delete(shopItem: ShopItem) {
        shopItemDao.deleteShopItem(shopItem)
    }

    override fun getAllShoppingItem(): LiveData<List<ShopItem>> {
       return shopItemDao.getAllShopItems()
    }

    override suspend fun search(img: String): Resource<ImageResultDto> {
        return try {
            val response = apiService.searchInImage(imageName = img)
            if (response.isSuccessful)
            {
                response.body()?.let {
                    return@let Resource.success(data = it)
                }?: Resource.error(msg = "unknown error occurd",data = null)
            }else
            {
                Resource.error("api not success",data = null)
            }
        }catch (e:Exception)
        {
            Resource.error(msg = "Error in api call",data = null)
        }
    }
}