package ir.shayan.okunittest.respository

import android.content.SharedPreferences


private const val HIGH_SCORE_KEY = "HIGH_SCORE_KEY"

class CocktailsRepositoryImpl(
    private val sharedPreferences: SharedPreferences
) : CocktailsRepository {
    override open fun saveHighScore(score: Int) {
        val editor = sharedPreferences.edit()
        editor.putInt(HIGH_SCORE_KEY, score)
        editor.apply()
    }

    override fun getHighScore(): Int =
        sharedPreferences.getInt(HIGH_SCORE_KEY, 0)
}