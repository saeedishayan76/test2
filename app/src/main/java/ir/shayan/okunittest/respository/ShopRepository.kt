package ir.shayan.okunittest.respository

import androidx.lifecycle.LiveData
import ir.shayan.okunittest.vo.Resource
import ir.shayan.okunittest.data.local.ShopItem
import ir.shayan.okunittest.data.remote.ImageResultDto

interface ShopRepository {
    suspend fun insert(shopItem: ShopItem)
    suspend fun delete(shopItem: ShopItem)
    fun getAllShoppingItem(): LiveData<List<ShopItem>>
    suspend fun search(img: String): Resource<ImageResultDto>
}