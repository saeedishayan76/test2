package ir.shayan.okunittest.respository

interface CocktailsRepository {
    open fun saveHighScore(score: Int)
    fun getHighScore():Int
}