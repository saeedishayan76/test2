package ir.shayan.okunittest.data.local

import androidx.lifecycle.LiveData
import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface ShopItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertShopItem(shopItem: ShopItem)

    @Delete
    suspend fun deleteShopItem(shopItem: ShopItem)

    @Query("SELECT * FROM ShopItem")
    fun getAllShopItems(): LiveData<List<ShopItem>>

    @Query("SELECT SUM(price * amount)  FROM ShopItem ")
    fun getTotalPrice(): LiveData<Float>
}