package ir.shayan.okunittest.data.remote

import ir.shayan.okunittest.BuildConfig
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/api/")
    suspend fun searchInImage(
        @Query("key") key: String = BuildConfig.API_KEY,
        @Query("q") imageName: String,
        @Query("image_type") imageType: String = "photo"
    ): Response<ImageResultDto>
}