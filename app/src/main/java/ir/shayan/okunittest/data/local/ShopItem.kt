package ir.shayan.okunittest.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ShopItem(
    var name:String ,
    var amount:Int,
    var price:Float,
    var image:String,
    @PrimaryKey(autoGenerate = true)
    var id :Int?= null
)