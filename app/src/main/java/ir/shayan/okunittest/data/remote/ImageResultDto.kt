package ir.shayan.okunittest.data.remote

data class ImageResultDto(
    val hits: List<Hit>,
    val total: Int,
    val totalHits: Int
)