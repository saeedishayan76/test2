package ir.shayan.okunittest.data.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [
        ShopItem::class
    ], version = 1
)
abstract class AppDataBase : RoomDatabase() {
    abstract fun shopItemDao(): ShopItemDao
}