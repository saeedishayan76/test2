package ir.shayan.okunittest.advance

data class ResResponse(
    val page: Int,
    val id: Int,
    val original_language: String,
    val original_title: String
)