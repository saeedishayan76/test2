package ir.shayan.okunittest.advance

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class VM : ViewModel() {

    val filterLiveData = MutableLiveData<String>()


    fun setFiltering(filter: Filter) {
        filterLiveData.value = filter.name
    }


    enum class Filter {
        ASC, DESC
    }
}


