package ir.shayan.okunittest.advance

data class Movie(
    val title: String, val poster_path: String, val overview: String
) {
}