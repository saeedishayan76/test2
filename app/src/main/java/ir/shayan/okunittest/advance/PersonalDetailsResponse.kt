package ir.shayan.okunittest.advance

data class PersonalDetailsResponse(
    val currentCity: String,
    val email: String,
    val name: String,
    val skills: String,
    val trainedAt: String)