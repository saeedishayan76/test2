package ir.shayan.okunittest.advance

import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("api/v1/employees")
    suspend fun getAllMovies(): Response<List<Movie>>

    @GET("movies.json")
    suspend fun getAllMoviessss(): PersonalDetailsResponse

}