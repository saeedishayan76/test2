package ir.shayan.okunittest.google

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.shayan.okunittest.vo.Event

class TaskViewModel : ViewModel() {
    private val _allTaskLiveData = MutableLiveData<Event<Boolean>>()
    val allTaskTaskLiveData: LiveData<Event<Boolean>> = _allTaskLiveData
    fun setFilter(ifAllTask: Boolean) {
        _allTaskLiveData.value = Event(ifAllTask)
    }
}