package ir.shayan.okunittest.google


fun main()
{
   val programmer = Programmer()
    while (programmer.isAlive()){
        programmer.eat()
        programmer.sleep()
        programmer.code()
        programmer.repeat()
        /** Happy Programmer's Day :)) */
    }
}