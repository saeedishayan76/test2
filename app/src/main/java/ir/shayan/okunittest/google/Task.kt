package ir.shayan.okunittest.google

data class Task(
    var title: String,
    var desc: String,
    var isCompleted: Boolean = false
) {


    val isActive: Boolean
        get() = !isCompleted
}

