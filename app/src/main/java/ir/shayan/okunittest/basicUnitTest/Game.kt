package ir.shayan.okunittest.basicUnitTest

open class Game(questions :List<Question>) {
    var score = 0

    fun incrementScore() {
        score++
    }

    open fun answer(question: Question, option: String) {
        question.answer(option)
        incrementScore()
    }
}