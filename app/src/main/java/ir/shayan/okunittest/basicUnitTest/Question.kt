package ir.shayan.okunittest.basicUnitTest

open class Question {
    var answeredOption: String? = "MY ANSWER"
        private set

    open fun answer(option: String):Boolean {
        answeredOption = option
        return true
    }
}