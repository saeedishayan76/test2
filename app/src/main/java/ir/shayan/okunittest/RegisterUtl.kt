package ir.shayan.okunittest

object RegisterUtil {

    val usernames = listOf("Ali", "Reza")

    /**
     * username is empty
     * password less than 2 digits
     * password and confirmPassword not matched
     */
    fun validateForSignUp(
        username: String,
        password: String,
        confirmPassword: String
    ): Boolean {
        if (username.isEmpty() || password.isEmpty())
            return false
        if (password != confirmPassword)
            return false
        if (username in usernames)
            return false
        return true
    }
}