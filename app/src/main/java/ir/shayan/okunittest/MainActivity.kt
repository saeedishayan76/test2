package ir.shayan.okunittest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private  val TAG = "MainActivityRun"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /**
         *
         * for show difference between run blocking vs coroutine scope
         */
//        CoroutineScope(Dispatchers.Main).launch {
//            Log.i(TAG, "onCreate: ${showNumber()}")
//            Log.i(TAG, "onCreate: ${showNumber()}")
//            Log.i(TAG, "onCreate: ${showNumber()}")
//            Log.i(TAG, "onCreate: ${showNumber()}")
//            Log.i(TAG, "onCreate: ${showNumber()}")
//
//        }
//
//
//        CoroutineScope(Dispatchers.Main).launch {
//            delay(1000L)
//            runBlocking {
//                Log.i(TAG, "onCreate: ${Thread.currentThread().name}")
//                Log.i(TAG, "onCreate: Start Run Blocking")
//                delay(4000L)
//                Log.i(TAG, "onCreate: End Run Blocking")
//            }
//        }

//
//
//        }


        lifecycleScope.launch {
            Log.i(TAG, "onCreate: ${Thread.currentThread().name}")

                delay(4000)
                Log.i(TAG, "onCreate: im")

        }
        Log.i(TAG, "onCreate: out")
        findViewById<TextView>(R.id.hre).setOnClickListener {
            Log.i(TAG, "onCreate: clicled")
        }
    }



}



suspend fun showNumber():Int
{
    delay(1000L)
    return Random.nextInt()
}