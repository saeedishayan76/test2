package ir.shayan

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TestViewModel :ViewModel() {
    val filterLiveData = MutableLiveData<String>()

    fun setFilter(filter:Filter){
        filterLiveData.value = filter.name

    }


    enum class Filter{
        ASC, DESC
    }
}