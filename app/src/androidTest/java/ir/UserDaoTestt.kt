package ir

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.google.common.truth.Truth.assertThat
import ir.shayan.ApppDataBase
import ir.shayan.User
import ir.shayan.UserDao
import ir.shayan.okunittest.data.local.AppDataBase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test

class UserDaoTestt {

    lateinit var appDataBase: ApppDataBase
    lateinit var userDao: UserDao


    @Before
    fun setup()
    {
        appDataBase = Room.inMemoryDatabaseBuilder(ApplicationProvider.getApplicationContext(),
        ApppDataBase::class.java).build()

        userDao = appDataBase.userDao()
    }

    @Test
    fun `InsertUserSuccess`() = runBlocking{
        val user = User("Ali",2357)
        userDao.insertUser(user)
        val result = userDao.getAllUsers().first()

        assertThat(result).contains(user)
    }

    @After
    fun tearDown()
    {
        appDataBase.close()
    }
}