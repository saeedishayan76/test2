package ir.shayan.okunittest

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.google.common.truth.Truth.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test

class ResourceCompareTest {

    // is a Bad Way
//    val resourceCompare = ResourceCompare()

    private lateinit var resourceCompare: ResourceCompare

    @Before
    fun setup() {
        resourceCompare = ResourceCompare()
    }

    @After
    fun afterTest() {

    }

    @Test
    fun `givenStringIsEqualToResourceString_returnTrue`() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val result = resourceCompare.isEqual(context, R.string.app_name, "OkUnitTest")
        assertThat(result).isTrue()
    }

    @Test
    fun `givenStringIsNotEqualToResourceString_returnFalse`() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val result = resourceCompare.isEqual(context, R.string.app_name, "Sample")
        assertThat(result).isFalse()
    }
}