package ir.shayan.okunittest.data.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.asLiveData
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject
import javax.inject.Named

@ExperimentalCoroutinesApi
@SmallTest
@HiltAndroidTest
class ShopItemDaoTest {

    @get:Rule
    var hiltAndroidRule = HiltAndroidRule(this)

    @get:Rule
    var instantTaskExcuterRoles = InstantTaskExecutorRule()

    @Inject
    @Named("for test")
     lateinit var appDataBase: AppDataBase
    private lateinit var shopItemDao: ShopItemDao

    @Before
    fun setup() {

        hiltAndroidRule.inject()
        shopItemDao = appDataBase.shopItemDao()
    }

    @After
    fun afterTest() {
        appDataBase.close()
    }


    @Test
    fun insertShopItem() = runBlockingTest {
        val shopItem = ShopItem("Banana", 1, 1000f, "img", 1)
        shopItemDao.insertShopItem(shopItem)
        val result = shopItemDao.getAllShopItems().getOrAwaitValue()
        assertThat(result).contains(shopItem)
    }

    @Test
    fun calculateTotal() = runBlockingTest {
        val shopItem1 = ShopItem("Banana", 1, 1000f, "img", 1)
        val shopItem2 = ShopItem("Apple", 2, 1000f, "img", 2)
        val shopItem3 = ShopItem("Orange", 3, 1000f, "img", 3)

        shopItemDao.insertShopItem(shopItem1)
        shopItemDao.insertShopItem(shopItem2)
        shopItemDao.insertShopItem(shopItem3)

        val totalItems = shopItemDao.getTotalPrice().getOrAwaitValue()

        assertThat(totalItems).isEqualTo(1000 + 2000 + 3000)


    }


}