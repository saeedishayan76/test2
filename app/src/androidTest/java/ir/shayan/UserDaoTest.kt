package ir.shayan

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import ir.shayan.okunittest.data.local.AppDataBase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

class UserDaoTest {

    lateinit var apppDataBase: ApppDataBase
    lateinit var userDao: UserDao


    @Before
    fun setup() {
        apppDataBase = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            ApppDataBase::class.java
        ).build()

        userDao = apppDataBase.userDao()
    }


    @Test
    fun `insertUser_whenFetch_returnTrue`() = runBlocking {

        val user = User("SHAYAN", 769)

        userDao.insertUser(user)

        val result = userDao.getAllUsers().first()

        assertThat(result).contains(user)

    }


    @Test
    fun `deleteUser_whenFetch_returnTrue`() = runBlocking {
        val user = User("SHAYAN", 769)


        userDao.deleteUser(user)

        val result = userDao.getAllUsers().first()

        assertThat(result).doesNotContain(user)

    }

    @After
    fun tearDown() {
        apppDataBase.close()
    }

}