package ir

import android.content.SharedPreferences
import ir.shayan.okunittest.respository.CocktailsRepositoryImpl
import org.junit.Test
import org.mockito.kotlin.*

class CocktalRepoUtilTest {


    @Test
    fun `saveScore_ shouldSaveInSharedPreferences`()
    {
        val sharedPrefencesEditor:SharedPreferences.Editor = mock()
        val shredPrefences:SharedPreferences = mock()

        whenever(shredPrefences.edit()).thenReturn(sharedPrefencesEditor)

        val repository = CocktailsRepositoryImpl(sharedPreferences = shredPrefences)

        val score = 100
        repository.saveHighScore(100)

        inOrder(sharedPrefencesEditor)
        {
            verify(sharedPrefencesEditor).putInt(any(), eq(score))
            verify(sharedPrefencesEditor).apply()
        }
    }

    @Test
    fun `last highScore_fromSharedPrefences`()
    {
        val sharedPreferences:SharedPreferences = mock()
        val repository = CocktailsRepositoryImpl(sharedPreferences)

        repository.getHighScore()

        verify(sharedPreferences).getInt(any() , any())
    }
}