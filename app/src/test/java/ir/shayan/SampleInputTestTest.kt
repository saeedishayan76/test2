package ir.shayan

import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

class SampleInputTestTest{


    @Test
    fun `When Signup ,userName or pass or confirm pass is empty then return false`(){
        val result = SampleInputTest.signUp("","sdsjdh","sdsjdh")
        assertThat(result).isFalse()
    }

    @Test
    fun `pass is not equals to confirm Pass`(){
        val result = SampleInputTest.signUp("Shayan","123","1234")
        assertThat(result).isFalse()
    }
}