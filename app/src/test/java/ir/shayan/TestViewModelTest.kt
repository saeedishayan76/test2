package ir.shayan

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import ir.shayan.okunittest.getOrAwaitValueTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class TestViewModelTest {


    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    lateinit var testViewModel: TestViewModel


    @Before
    fun setup()
    {
        testViewModel = TestViewModel()

    }

    @Test
    fun `When Filter ASC then filterLiveData value is Asc`() {
        testViewModel.setFilter(TestViewModel.Filter.ASC)
        val result = testViewModel.filterLiveData.getOrAwaitValueTest()
        assertThat(result).isEqualTo(TestViewModel.Filter.DESC.name)

    }
}