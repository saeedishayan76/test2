package ir.shayan.okunittest.advance

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.ViewModel
import com.google.common.truth.Truth.assertThat
import ir.shayan.okunittest.getOrAwaitValueTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class VMTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    lateinit var vm: VM

    @Before
    fun setup() {
        vm = VM()
    }

    @Test
    fun `When Set Filter to ASC then return LiveData ASC`() {

        vm.setFiltering(VM.Filter.ASC)

        val result = vm.filterLiveData.getOrAwaitValueTest()

        assertThat(result).isEqualTo(VM.Filter.ASC.name)

    }

    @Test
    fun `When Filter to DESC Then LiveData to DESC`() {

        vm.setFiltering(VM.Filter.DESC)
        val result = vm.filterLiveData.getOrAwaitValueTest()
        assertThat(result).isEqualTo(VM.Filter.DESC.name)

    }
}