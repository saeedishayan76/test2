package ir.shayan.okunittest.advance

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import ir.shayan.okunittest.getOrAwaitValueTest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

class MainViewModelTest {

    lateinit var mainViewModel: MainViewModel
    lateinit var mainRepository: MainRepository
    private val testDispatcher = TestCoroutineDispatcher()

    @get:Rule
    val instantTaskExecutionRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var apiService: ApiService

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        mainRepository = MainRepository((apiService))
        mainViewModel = MainViewModel(mainRepository)
    }

//    @Test
//    fun `getAllMovies_Success`() = runBlocking {
//
//            Mockito.`when`(apiService.getAllMovies()).thenReturn(Response.success(listOf<Movie>()))
//            mainViewModel.getAllMovies()
//
//            val result = mainViewModel.movieList.getOrAwaitValueTest  ()
//
//        assertThat(result).isEqualTo(listOf<Movie>(Movie("ashadsj","hsdjshd")))
//        }
}