package ir.shayan.okunittest.advance

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import ir.MainCoroutineRule
import ir.shayan.okunittest.getOrAwaitValueTest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

@ExperimentalCoroutinesApi
class MViewModelTest{

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()


    lateinit var mainViewModel: MainViewModel
    @Mock
    lateinit var mainRepository: MainRepository

    @Before
    fun setup()
    {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(UnconfinedTestDispatcher() )
        mainViewModel = MainViewModel(mainRepository)
    }

    @Test
    fun `givenViewModelWhenFetchAllMoviesThenSuccess`() = runTest{

        Mockito.`when`(mainRepository.getAllMovies()).thenReturn(Response.success(emptyList()))

       mainViewModel.getAllMovies()

        val result = mainViewModel.movieList.getOrAwaitValueTest()

        assertThat(result).isEqualTo(emptyList<Movie>())
    }
}