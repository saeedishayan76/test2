package ir.shayan.okunittest.advance

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.google.gson.Gson
import com.google.gson.JsonParser
import ir.MockResponseFileReader
import ir.enqueueResponse
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainRepositoryTest {


    lateinit var mainRepository: MainRepository
    lateinit var apiService: ApiService

    lateinit var mockWebServer: MockWebServer



    @Before
    fun setup() {
        mockWebServer = MockWebServer()

        apiService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
        mainRepository = MainRepository(apiService)



    }


    @Test
    fun `givenFileWhenRead_returnTrue`() {
        val result = MockResponseFileReader("res.json")
        assertThat(result.content).isNotEmpty()
    }

    @Test
    fun `givenRepository WhenApiCall Success`() = runBlocking {

        val response = MockResponse()
            .setBody(MockResponseFileReader("res.json").content)
            .setResponseCode(200)

        mockWebServer.enqueue(response)
        val expected = apiService.getAllMoviessss()


        assertThat(expected).isEqualTo(Gson().fromJson(MockResponseFileReader("res.json").content,PersonalDetailsResponse::class.java))
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }
}