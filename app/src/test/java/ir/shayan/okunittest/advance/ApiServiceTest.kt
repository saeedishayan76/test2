package ir.shayan.okunittest.advance

import com.google.common.truth.Truth.assertThat
import com.google.gson.Gson
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiServiceTest {

    lateinit var mockWebServer: MockWebServer

    lateinit var apiService: ApiService

    lateinit var gson: Gson

    @Before
    fun setup()
    {
        MockitoAnnotations.openMocks(this)
        gson = Gson()
        mockWebServer = MockWebServer()
        mockWebServer.start()
        apiService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build().create(ApiService::class.java)
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }


    @Test
    fun `given ApiService when api call then result`() = runBlocking{
        val mockResponse = MockResponse()
        mockResponse.setBody("[]")
        mockWebServer.enqueue(mockResponse)
        val response = apiService.getAllMovies()
       assertThat(response).isEqualTo("[]")

//        assertThat(response.body()?.isEmpty() == true).isTrue()
//        assertThat(request.path).isEqualTo("/movielist.json")
    }
}