package ir.shayan.okunittest

import com.google.common.truth.Truth.assertThat
import org.junit.Test

class RegisterUtilTest{

    @Test
    fun `empty user name return false`(){
        val result = RegisterUtil.validateForSignUp(
            "",
            "1234",
            "1234"
        )
        assertThat(result).isTrue()
    }

    @Test
    fun `valid user name and match password return true`(){
        val result = RegisterUtil.validateForSignUp(
            "Shayan",
            "1234",
            "1234"
        )
        assertThat(result).isTrue()
    }

    @Test
    fun `password not match with confirmPassword retrun false`(){
        val result = RegisterUtil.validateForSignUp(
            "Shayan",
            "12909",
            "12340"
        )
        assertThat(result).isFalse()
    }

    @Test
    fun `username already exits retrun false`(){
        val result = RegisterUtil.validateForSignUp(
            "Ali",
            "12909",
            "12340"
        )
        assertThat(result).isFalse()
    }
}