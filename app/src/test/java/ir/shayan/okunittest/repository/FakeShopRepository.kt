package ir.shayan.okunittest.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ir.shayan.okunittest.vo.Resource
import ir.shayan.okunittest.data.local.ShopItem
import ir.shayan.okunittest.data.remote.ImageResultDto
import ir.shayan.okunittest.respository.ShopRepository

class FakeShopRepository : ShopRepository {

    private val shopItems = mutableListOf<ShopItem>()
    private val shopItemsLiveData = MutableLiveData<List<ShopItem>>()
    private var shouldNetworkError = false

    fun setShouldNetworkError(value:Boolean)
    {
        shouldNetworkError = value
    }
    override suspend fun insert(shopItem: ShopItem) {
        shopItems.add(shopItem)
        refreshShopItemLiveData()
    }

    override suspend fun delete(shopItem: ShopItem) {
        shopItems.remove(shopItem)
        refreshShopItemLiveData()
    }

    override fun getAllShoppingItem(): LiveData<List<ShopItem>> {
        return shopItemsLiveData
    }

    fun refreshShopItemLiveData() {
        shopItemsLiveData.postValue(shopItems)
    }

    override suspend fun search(img: String): Resource<ImageResultDto> {
            return if (shouldNetworkError)
            {
                Resource.error(msg = "Error",data = null)
            }else
            {
                Resource.success(ImageResultDto(emptyList(),0,0))
            }
    }


}