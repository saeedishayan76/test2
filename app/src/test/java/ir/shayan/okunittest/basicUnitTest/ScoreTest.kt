package ir.shayan.okunittest.basicUnitTest

import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.mockito.kotlin.mock

class ScoreTest{



    @Test
    fun `check current when increment highest`()
    {
        val score  = Score(5)
        score.increment()
        assertThat(score.highest).isEqualTo(5)

    }
}