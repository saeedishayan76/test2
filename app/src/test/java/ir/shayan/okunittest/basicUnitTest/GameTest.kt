package ir.shayan.okunittest.basicUnitTest

import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.kotlin.eq
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.lang.reflect.GenericArrayType

class GameTest {


    @Test
    fun `check score increment working successFully`() {
        val game = Game(emptyList())
        game.incrementScore()

        assertThat(game.score).isEqualTo(1)

    }

    @Test
    fun `whenAnswering_shouldDelegateToQuestion`() {
        val question = mock<Question>()
        val game = Game(listOf(question))
        game.answer(question, "OPTION")

        verify(question).answer(eq("OPTION"))
    }

    @Test
    fun `with any string should be increment score`() {
        val question = mock<Question>()
        Mockito.`when`(question.answer(anyString())).thenReturn(true)
        val game = Game(listOf(question))
        game.answer(question,"OPTION")

        assertThat(game.score).isEqualTo(1)
    }
}