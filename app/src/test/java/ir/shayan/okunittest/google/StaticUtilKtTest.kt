package ir.shayan.okunittest.google

import com.google.common.truth.Truth.assertThat
import org.junit.Test

class StaticUtilKtTest{



    @Test
    fun `getActiveAndCompletedStats_withNoCompletedReturnZeroHundred`()
    {
        val tasks = mutableListOf<Task>()
        tasks.add(Task("First","Testing",isCompleted = false))

        val result = getActiveAndCompletedStats(tasks)

        assertThat(result.activeTasksPercent).isEqualTo(100f)
        assertThat(result.completedTasksPercent).isEqualTo(0f)

    }

    @Test
    fun `getActiveAndCompletedStats_emptyList_returnZeroZero`()
    {
        val result = getActiveAndCompletedStats(emptyList())
        assertThat(result.completedTasksPercent).isEqualTo(0f)
        assertThat(result.activeTasksPercent).isEqualTo(0f)

    }
}