package ir.shayan.okunittest.google

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import ir.shayan.okunittest.getOrAwaitValueTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class TaskViewModelTest {
    lateinit var taskViewModel: TaskViewModel

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        taskViewModel = TaskViewModel()
    }

    @Test
    fun `allTaskLiveData_whenFilterToAll_returnTrue`() {
        taskViewModel.setFilter(true)

        val result = taskViewModel.allTaskTaskLiveData.getOrAwaitValueTest()

        assertThat(result.getContentIfNotHandled()).isTrue()
    }
}