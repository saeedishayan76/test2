package ir.shayan.okunittest

import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ValidateUtilTest{


    @Test
    fun `when username or pass or confirmPass empty then return False`(){

        val result = ValidateUtil.validateForSignup("","sdksdk","sdksdk")
        assertThat(result).isFalse()
    }

    @Test
    fun `when pass and confirmPass not Equals then return false`(){

        val result = ValidateUtil.validateForSignup("Shayan","123","1223")
        assertThat(result).isFalse()
    }

}