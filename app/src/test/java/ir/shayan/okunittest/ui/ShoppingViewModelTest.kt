package ir.shayan.okunittest.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import ir.shayan.okunittest.data.local.ShopItem
import ir.shayan.okunittest.data.remote.ApiService
import ir.shayan.okunittest.getOrAwaitValueTest
import ir.shayan.okunittest.repository.FakeShopRepository
import ir.shayan.okunittest.vo.Resource
import ir.shayan.okunittest.vo.Status
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ShoppingViewModelTest {

    private val mockWebServer = MockWebServer()
    private val api = Retrofit.Builder()
        .baseUrl(mockWebServer.url("https://pixabay.com"))
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiService::class.java)
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var shoppingViewModel: ShoppingViewModel

    @Before
    fun setup() {
        shoppingViewModel = ShoppingViewModel(FakeShopRepository())
    }

    @Test
    fun `insert field with empty field return false`() {
        shoppingViewModel.insertShoppingItem("name", "", "2100")

        val result = shoppingViewModel.insertShoppingItemStatus.getOrAwaitValueTest()
        assertThat(result.getContentIfNotHandled()?.status).isEqualTo(Status.ERROR)
    }

    @Test
    fun `insert field with bigAmount field return false`() {
        shoppingViewModel.insertShoppingItem("name", "23455", "2100")

        val result = shoppingViewModel.insertShoppingItemStatus.getOrAwaitValueTest()
        assertThat(result.getContentIfNotHandled()?.status).isEqualTo(Status.ERROR)
    }

    @Test
    fun apiCall()
    {

    }
}