package ir.shayan

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import ir.MainCoroutineRule
import ir.shayan.okunittest.getOrAwaitValueTest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class UserViewModelTest{

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()


    @Before
    fun setup()
    {
        Dispatchers.setMain(UnconfinedTestDispatcher())
    }
    @Test
    fun `givenViewModel_WhenSetFilter_`(){

        val userViewModel = UserViewModel()
        userViewModel.setFilter(UserViewModel.Filter.DESC)
        val result = userViewModel.isFilterLiveData.getOrAwaitValueTest()

        assertThat(result).isEqualTo("DESC")
    }

    @Test
    fun `given ViewModel When SampleCall Then`() = runBlocking{
        val userViewModel = UserViewModel()
        userViewModel.sample()
        val result = userViewModel.isFilterLiveData.getOrAwaitValueTest()
        assertThat(result).isEqualTo("Test")
    }
}