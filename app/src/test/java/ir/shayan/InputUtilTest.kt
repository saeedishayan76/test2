package ir.shayan

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import org.junit.Rule
import org.junit.Test

class InputUtilTest{



    @Test
    fun `givenValidateUserPass_returnFalse`(){

        val result = InputUtil.validateUserPass("","")
        assertThat(result).isFalse()
    }

    @Test
    fun `givenValidatePass_WhenPassIsEmpty_returnFalse`(){
        val result = InputUtil.validateUserPass("ahsghasg","")
        assertThat(result).isFalse()
    }
}